from django.contrib import admin
from .models.evento import Evento
from .models.almacen import Almacen
from .models.tiendaMasPopular import TiendaMasPopular
from .models.tipo_almacen import Tipo_Almacen

# Register your models here.

class TipoAdmin(admin.ModelAdmin):
    list_per_page=20
    list_display=('id','nombreAlmacen','tipoAlmacen')
    search_fields=['tipoAlmacen']
    list_filter=('tipo_almacen',)


admin.site.register(Evento)
admin.site.register(Almacen)
admin.site.register(TiendaMasPopular)
admin.site.register(Tipo_Almacen)
