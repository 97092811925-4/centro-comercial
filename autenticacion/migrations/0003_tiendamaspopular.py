# Generated by Django 3.2.8 on 2021-10-08 04:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('autenticacion', '0002_almacen'),
    ]

    operations = [
        migrations.CreateModel(
            name='TiendaMasPopular',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('nombreAlmacén', models.CharField(max_length=30)),
                ('visitasMensuales', models.IntegerField()),
            ],
        ),
    ]
