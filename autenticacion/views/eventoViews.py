from rest_framework import views, status
from rest_framework import generics 
from rest_framework.response import Response
from autenticacion.models import Evento
from autenticacion.Serializadores import EventoSerializador

# listar 
class EventoListCreateView(generics.ListCreateAPIView):
    queryset = Evento.objects.all()
    serializer_class= EventoSerializador

# leer actualizar eliminar 
class EventoRetrieveUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset =Evento.objects.all()
    serializer_class= EventoSerializador 