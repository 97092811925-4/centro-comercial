from django.db.models import query
from rest_framework import views, status
from rest_framework import generics 
from rest_framework.response import Response
from autenticacion.models import Tipo_Almacen
from autenticacion.Serializadores.tipo_almacenSerializador  import TipoAlmacenZerializador

## crear - listar 

class TipoAlmacenListCreateView(generics.ListCreateAPIView):
    querySet = Tipo_Almacen.objects.all()
    serializer_class = TipoAlmacenZerializador

## eliminar y actualizar 

class TipoAlmacenRetriveUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset= Tipo_Almacen.objects.all()
    serializer_class = TipoAlmacenZerializador