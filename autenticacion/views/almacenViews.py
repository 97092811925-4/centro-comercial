from rest_framework import views, status
from rest_framework.response import Response
from autenticacion.models import Almacen
from autenticacion.Serializadores import AlmacenSerializador
from rest_framework import generics 
# listar 
class AlmacenListCreateView(generics.ListCreateAPIView):
    queryset = Almacen.objects.all()
    serializer_class= AlmacenSerializador

# leer actualizar eliminar 
class AlmacenRetrieveUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset =Almacen.objects.all()
    serializer_class= AlmacenSerializador 
