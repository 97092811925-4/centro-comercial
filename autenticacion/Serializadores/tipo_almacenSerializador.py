from rest_framework import serializers
from autenticacion.models.tipo_almacen import Tipo_Almacen

class TipoAlmacenZerializador(serializers.ModelSerializer): #ModelSerializers
    class Meta:
        model = Tipo_Almacen
        fields =['nombreAlmacen','tipoAlmacen']