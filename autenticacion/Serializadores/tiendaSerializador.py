from rest_framework import Serializadores
from autenticacion.models.tiendaMasPopular import TiendaMasPopular


class tiendaserializedor(Serializadores.ModelSerializador):
    class Meta:
        model = TiendaMasPopular
        fields = ['nombreAlmacén', 'visitasMensuales']