from rest_framework import serializers
from autenticacion.models.almacen import Almacen

class AlmacenSerializador(serializers.ModelSerializer):
    class Meta:
        model = Almacen
        fields = ['nombreAlmacen','numeroLocal']