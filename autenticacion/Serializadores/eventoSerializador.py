from rest_framework import serializers
from autenticacion.models.evento import Evento

class EventoSerializador(serializers.ModelSerializer):
    class Meta:
        model = Evento
        fields = ['clase','cantidadAsistentes']