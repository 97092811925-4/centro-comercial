from django.db import models
class Almacen(models.Model):
    id = models.AutoField(primary_key=True)
    nombreAlmacen = models.CharField(max_length=30)
    numeroLocal = models.IntegerField()