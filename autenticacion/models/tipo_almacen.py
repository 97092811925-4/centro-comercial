from django.db import models 
from .almacen import Almacen

class Tipo_Almacen(models.Model):

    id=models.AutoField(primary_key=True)
    nombreAlmacen=models.ForeignKey(
        Almacen,
        related_name="lista_alamacenes",
        on_delete=models.SET_NULL,
        null=True
        )
    
    tipoAlmacen=models.CharField(
        max_length=50,
        choices=(            
            ('ROPA','Tiendas de Ropa'),
            ('DEPORTE','Tiendas de deportes'),
            ('ELECTRO','Tiendas de electrodomesticos'),
            ('ESCOLAR','Tiendas escolares') ,
            ('CINE','Cines'),
            ('FARMACIA','Farmacias'), 
            ('VIDEOJ','Tiendas de Video Juegos'),
            ('COMIDA','Zona de comidas')
        )
    )  
  
