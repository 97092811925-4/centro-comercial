from django.db import models
class TiendaMasPopular(models.Model):
    id = models.AutoField(primary_key=True)
    nombreAlmacén = models.CharField(max_length=30)
    visitasMensuales = models.IntegerField()