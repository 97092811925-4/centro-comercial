from django.db import models

class Evento(models.Model):
    id = models.AutoField(primary_key=True)
    clase = models.CharField(max_length=30)
    cantidadAsistentes = models.IntegerField()