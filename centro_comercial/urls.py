from django.contrib import admin
from django.urls import path
from autenticacion.views import AlmacenListCreateView, AlmacenRetrieveUpdateDeleteView, EventoListCreateView, EventoRetrieveUpdateDeleteView
from autenticacion.views.tipoAlmaceViews import TipoAlmacenListCreateView, TipoAlmacenRetriveUpdateDeleteView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('almacenes/', AlmacenListCreateView.as_view()),
    path('almacen/<int:pk>/', AlmacenRetrieveUpdateDeleteView.as_view()),
    path('eventos/', EventoListCreateView.as_view()),
    path('eventos/<int:pk>', EventoRetrieveUpdateDeleteView.as_view()),
    path('tipo/', TipoAlmacenListCreateView.as_view()),
    path('tipo/<int:pk>/', TipoAlmacenRetriveUpdateDeleteView.as_view())

]
